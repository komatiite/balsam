------------------------------------------------------------------------
-- INSERT MOCK DATA SCRIPT
-- Description:		Inserts mock data into the Balsam tables
-- Author:			Kent Crozier
-- Created: 		December 1, 2016
-- Updated:			December 1, 2016
-- Comments:		
------------------------------------------------------------------------

/*
INSERT INTO Users
			(JoinDate, UserName, UserPass, FirstName, Email, Avatar)
	VALUES	(CURRENT_DATE, 'kcrozier', 'passwd', 'Kent', 'kcrozier@gmail.com', 'kcrozier.jpg');

INSERT INTO LifeEvents
			(DateCreated, DateEnded, EventName, Description, Location)
	VALUES	(CURRENT_DATE, '9999-12-31', 'Creating Balsam',
	 		'The development of the initial Balsam platform', 'Winnipeg');
            
INSERT INTO Posts
			(PostDate, Author, PostSubject, TextContent, Image, Sound, Video, FileAttach, EventID)
	VALUES	(CURRENT_DATE, '1', 'Test Subject Line', '1', '0', '0', '0', '0', '1');

INSERT INTO TextContent
			(TextDate, PostID, Content)
	VALUES	(CURRENT_DATE, '2', 'This is some test text of a text post! Wheeee, it works!');
*/
INSERT INTO Thumbnails
			(ThumbDate, PostID, Filename)
	VALUES	(CURRENT_DATE, '2', 'testthumb.jpg');

