------------------------------------------------------------------------
-- CREATE TABLES SCRIPT
-- Description:		Creates the tables for the Balsam database
-- Author:			Kent Crozier
-- Created: 		November 26, 2016
-- Updated:			December 1, 2016
-- Comments:		Users created 11/26, all other tables created 12/01.
------------------------------------------------------------------------
DROP TABLE TextContent;
DROP TABLE Images;
DROP TABLE SoundFiles;
DROP TABLE Videos;
DROP TABLE Files;
DROP TABLE Thumbnails;
DROP TABLE PostsCategories;
DROP TABLE PostsTags;
DROP TABLE Categories;
DROP TABLE Tags;
DROP TABLE Posts;
DROP TABLE Users;
DROP TABLE LifeEvents;

CREATE TABLE Users
(
	UserID 		TINYINT AUTO_INCREMENT 	NOT NULL,
	JoinDate 	DATE 					NOT NULL,
	UserName	VARCHAR(20) 			NOT NULL,
	UserPass	VARCHAR(20) 			NOT NULL,
	FirstName	VARCHAR(20) 			NOT NULL,
	Email		VARCHAR(40) 			NOT NULL,
	Avatar		VARCHAR(20),
	CONSTRAINT	UsersPK
		PRIMARY KEY(UserID)
);

CREATE TABLE LifeEvents
(
	EventID			SMALLINT AUTO_INCREMENT		NOT NULL,
	DateCreated		DATE 						NOT NULL,
	DateEnded		DATE 						NOT NULL,
	EventName 		VARCHAR(255)				NOT NULL,
	Description		VARCHAR(255)				NOT NULL,
	Location		VARCHAR(255)				NOT NULL,
	CONSTRAINT 		EventsPK
		PRIMARY KEY(EventID)
);

CREATE TABLE Posts
(
	PostID		INT AUTO_INCREMENT 		NOT NULL,
	PostDate	DATE 					NOT NULL,
	Author		TINYINT					NOT NULL,
	PostSubject	VARCHAR(500)			NOT NULL,
	TextContent	BOOLEAN					NOT NULL,
	Image		BOOLEAN					NOT NULL,
	Sound		BOOLEAN					NOT NULL,
	Video		BOOLEAN					NOT NULL,
	FileAttach	BOOLEAN					NOT NULL,
	EventID		SMALLINT				NOT NULL,
	CONSTRAINT	PostsPK
		PRIMARY KEY(PostID),
	CONSTRAINT	WriteFK
		FOREIGN KEY(Author)
		REFERENCES Users(UserID),
	CONSTRAINT	SchedulesFK
		FOREIGN KEY(EventID)
		REFERENCES LifeEvents(EventID)
);

CREATE TABLE TextContent
(
	TextDate	DATETIME				NOT NULL,
	PostID		INT 					NOT NULL,
	Content 	TEXT,
	CONSTRAINT 	TextContentPK
		PRIMARY KEY(TextDate),
	CONSTRAINT 	ReadsFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE Images
(
	ImageDate	DATETIME				NOT NULL,
	PostID		INT 					NOT NULL,
	Filename	VARCHAR(40)				NOT NULL,
	Title		VARCHAR(255),
	Description	TEXT,
	CONSTRAINT	ImagesPK
		PRIMARY KEY(ImageDate),
	CONSTRAINT	ShowsFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE SoundFiles
(
	SoundDate	DATETIME 				NOT NULL,
	PostID		INT 					NOT NULL,
	Filename	VARCHAR(40)				NOT NULL,
	Title		VARCHAR(255),
	Description	TEXT,
	CONSTRAINT 	SoundsPK
		PRIMARY KEY(SoundDate),
	CONSTRAINT 	VoicesFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE Videos
(
	VideoDate	DATETIME				NOT NULL,
	PostID		INT 					NOT NULL,
	YouTube		BOOLEAN					NOT NULL,
	Filename	VARCHAR(50)				NOT NULL,
	Title		VARCHAR(255),
	Description	TEXT,
	CONSTRAINT	VideosPK
		PRIMARY KEY(VideoDate),
	CONSTRAINT 	PlaysFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE Files
(
	FileDate	DATETIME				NOT NULL,
	PostID		INT 					NOT NULL,
	Filename	VARCHAR(40)				NOT NULL,
	Title		VARCHAR(255),
	Description	TEXT,
	CONSTRAINT	FilesPK
		PRIMARY KEY(FileDate),
	CONSTRAINT 	AttachesFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE Thumbnails
(
	ThumbDate	DATETIME				NOT NULL,
	PostID		INT 					NOT NULL,
	Filename	VARCHAR(40)				NOT NULL,
	CONSTRAINT 	ThumbnailsPK
		PRIMARY KEY(ThumbDate),
	CONSTRAINT 	DisplaysFK
		FOREIGN KEY(PostID)
		REFERENCES Posts(PostID)
);

CREATE TABLE Categories
(
	CatID		SMALLINT AUTO_INCREMENT	NOT NULL,
	CatDate		DATETIME				NOT NULL,
	CatName		VARCHAR(50)				NOT NULL,
	CONSTRAINT 	CategoriesPK
		PRIMARY KEY(CatID)
);

CREATE TABLE Tags
(
	TagID		SMALLINT AUTO_INCREMENT	NOT NULL,
	TagDate		DATETIME				NOT NULL,
	TagName		VARCHAR(50)				NOT NULL,
	CONSTRAINT 	TagsPK
		PRIMARY KEY(TagID)
);

CREATE TABLE PostsCategories
(
	PostID		INT 					NOT NULL,
	CatID		SMALLINT				NOT NULL,
	CONSTRAINT 	PostsCategoriesPK
		PRIMARY KEY(PostID, CatID),
	CONSTRAINT 	CategorizesFK
		FOREIGN KEY(PostID)
        REFERENCES Posts(PostID),
	CONSTRAINT 	CategorizedFK
		FOREIGN KEY(CatID)
        REFERENCES Categories(CatID)			
);

CREATE TABLE PostsTags
(
	PostID		INT 					NOT NULL,
	TagID		SMALLINT				NOT NULL,
	CONSTRAINT 	PostsTagsPK
		PRIMARY KEY(PostID, TagID),
	CONSTRAINT 	TagsFK
		FOREIGN KEY(PostID)
        REFERENCES Posts(PostID),
	CONSTRAINT 	TaggedFK
		FOREIGN KEY(TagID)
		REFERENCES Tags(TagID)
);