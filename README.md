# BALSAM README #

Welcome to my Balsam project!

### What is Balsam? ###

* Balsam is a simple social media platform to be used with close friends and family in documenting the day to day events and activities that make up one's life. 
* Kent Crozier started Balsam in November 2016, and the project uses HTML5, CSS, JavaScript, and PHP on a Ubuntu Linux backend with an Apache webserver and MySQL database. 
* The project will be deployed to the Web when basic functionality and security is ensured.

### How do I get set up? ###

* Balsam employs a LAMP (Linux-Apache-MySQL-PHP) stack and is intended to be used on such.

### Contribution guidelines ###

* No contributions will be accepted, though developers are free to browse, download, and progress the code for their own purposes.

### Who do I talk to? ###

* Please contact Kent Crozier at kent.crozier@gmail.com 'Attn: Balsam' if you have any questions relating to the project.