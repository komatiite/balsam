<html>
<head>
	<title>User Database</title>
	<link rel="stylesheet" type="text/css" href="createuser.css"/>
</head>
<body>


<?php

	// Prepare new row entry
	$date = date('Y-m-d');
	$name = $_POST['name'];
	$user_name = $_POST['username'];
	$password = $_POST['password'];
	$email = $_POST['email'];

	// Connect to the Floofhouse database
	$floof_db = new mysqli("localhost", "floof", "floof", "Floofhouse");

	// Build insert query
	$insert_query = "INSERT INTO Users (JoinDate, UserName, Password, Name, Email)
	VALUES('$date', '$user_name', '$password', '$name', '$email')";

	// Execute insert query
	mysqli_query($floof_db, $insert_query);
	
	// Get rows from table
	$table_rows = $floof_db->query("SELECT * FROM Users");
	$number_rows = $table_rows->num_rows;

	

	// Create table
	echo "<table>";
	echo "<tr class='bold'>";
	echo "<td>UserID</td>";
	echo "<td>JoinDate</td>";
	echo "<td>UserName</td>";
	echo "<td>Password</td>";
	echo "<td>Name</td>";
	echo "<td>Email</td>";
	echo "<td>Avatar</td>";
	echo "</tr>";

	// Cycle through rows
	for ($i = 0; $i < $number_rows; $i++)
	{
		// Get current row
		$row = $table_rows->fetch_assoc();

		echo "<tr>";
		echo "<td>" . $row['UserID'] . "</td>";
		echo "<td>" . $row['JoinDate'] . "</td>";
		echo "<td>" . $row['UserName'] . "</td>";
		echo "<td>" . $row['Password'] . "</td>";
		echo "<td>" . $row['Name'] . "</td>";
		echo "<td>" . $row['Email'] . "</td>";
		echo "<td>" . $row['Avatar'] . "</td>";
		echo "</tr>";
	}

	echo "</table>";

	$floof_db->close();

?>
</body>
</html>